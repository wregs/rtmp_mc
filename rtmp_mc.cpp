#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <vector>

//#define PORT_RTMP	1935
//#define SERVER_ADDR	"mtg.babahh.com"

int sockfd;
static const char* server_addr="mtg.babahh.com";//"media.err.ee";
static const int server_port=1935;//80; 
static const char* rtmp_app="";



typedef unsigned char byte;
typedef unsigned long ulong;
typedef byte	h_0;
typedef struct _h_1{byte time[4];byte  zero[4];byte random[1528];} h_1;
typedef struct _h_2{byte time[4];byte time2[4];byte random[1528];} h_2;	
#define set_rtmp_basic_header(fmt,csid)	((fmt<<6)|csid)

#define rtmp_header_type0	0x00
#define rtmp_header_type1	0x01
#define rtmp_header_type2	0x02
#define rtmp_header_type3	0x03

#define rtmp_amf_number		0x00
#define rtmp_amf_boolean	0x01
#define rtmp_amf_string		0x02
#define rtmp_amf_object		0x03
#define	rtmp_amf_null		0x05

#define rtmp_amf0_command	0x14
#define rtmp_window_ack		0x05
#define rtmp_set_bandwidth	0x06
#define rtmp_user_ctrl_msg	0x04
#define rtmp_audio_packet	0x08
#define rtmp_video_packet	0x09
#define rtmp_set_chunk_size	0x01

#define rtmp_audio_chunk_size	0x40
#define rtmp_video_chunk_size	0x80

#define rtmp_ctrl_msg_sbegin	0x0000


static byte rtmp_chuck_size=rtmp_video_chunk_size;

int recvall(const int &sockfd,byte* data,const size_t data_size,int flag);

void tohex(byte *data,int data_size,ulong val)
{
	memset(data,0,data_size);
	for(int i=0;i<data_size;i++)
	{
		*(data+(data_size-i-1))=*(((byte*)&val)+i);
	}
}

void tohex(byte *data,int data_size,double val)
{
	memset(data,0,data_size);
	for(int i=0;i<data_size;i++)
	{
		*(data+(data_size-i-1))=*(((byte*)&val)+i);
	}
}

ulong fromhex(const byte* data, int data_size)
{
	ulong val=0;
	byte *pval =(byte*)&val;
	
	for(int i=0;i<data_size;i++)
	{
		*(pval+i)=*(data+data_size-i-1);
	}
	
	if(sizeof(ulong)>data_size)
		memset(pval+data_size,0,sizeof(ulong)-data_size);
	return val;
}

class rtmp_type{
public:
	byte amf_type;
	byte* data;
	size_t data_size;
public:
	rtmp_type(byte val):amf_type(val),data(0),data_size(0){}
	virtual ~rtmp_type(){if(data) delete[] data;}
	virtual byte* getData()=0;
	virtual size_t getDataSize()=0;
	}
};
class rtmp_string:public rtmp_type{
public:
	byte length[2];
public:
	rtmp_string(const char* val,size_t size):rtmp_type(0x02){
		data_size=size+1+2;		
		memset(length,0,2);
		tohex(length,2,(ulong)size);
		data=new byte[size+1+2];
		memcpy(data,&amf_type,1);
		memcpy(data+1,length,2);
		memcpy(data+1+2,val,size);
		data_size=size+1+2;
	}
	~rtmp_string(){}
	
	byte* getData(){return data;}
	size_t getDataSize(){return data_size;}
	
};

class rtmp_number:public rtmp_type{
public:
	rtmp_number(double val):rtmp_type(0x00){
		data=new byte[8+1];
		memcpy(data,&amf_type,1);
		tohex(data+1,8,val);
		data_size=9;	
	}
	~rtmp_number(){}
	byte* getData(){return data;}
	size_t getDataSize(){return data_size;}
	static rtmp_number* parse_type(byte* data){
		ulong val=fromhex(data+1,8);
		
		return new rtmp_number(val);
	}
};

class rtmp_boolean:public rtmp_type{
public:
	rtmp_boolean(byte val):rtmp_type(0x01){
		data=new byte[1+1];
		memcpy(data,&amf_type,1);
		memcpy(data+1,&val,1);
		data_size=2;	
	}
	~rtmp_boolean(){}
	byte* getData(){return data;}
	size_t getDataSize(){return data_size;}
	static rtmp_boolean* parse_boolean(byte* data){
		ulong val=fromhex(data+1,1);

		return new rtmp_boolean((byte)val);
	}
};

class rtmp_null:public rtmp_type{
public:
	rtmp_null():rtmp_type(0x05){
		data=new byte[1];
		memcpy(data,&amf_type,1);
		data_size=1;
	}
	~rtmp_null(){}
	byte* getData(){return data;}
	size_t getDataSize(){return data_size;}
	static rtmp_null* parse_null(){
	
		return new rtmp_null();
	}
};

class rtmp_name{
public:
	byte length[2];
	byte* data;
	size_t data_size;
public:
	rtmp_name(const char* val,size_t size):data_size(size+2){
		memset(length,0,2);
		tohex(length,2,(ulong)size);
		data=new byte[size+2];
		memcpy(data,length,2);
		memcpy(data+2,val,size);		
	}
	~rtmp_name(){delete[] data;}
	byte* getData(){return data;}
	size_t getDataSize(){return data_size;}
};

class rtmp_property{
public:
	rtmp_name *name;
	rtmp_type *type;
	byte* data;
	size_t data_size;
public:
	rtmp_property(rtmp_name *name,rtmp_type *type):name(name),type(type),data(0),data_size(0){}
	~rtmp_property(){delete name;delete type; if(data) delete[] data;}
	byte* getData(){
		if(!data){
			data=new byte[getDataSize()];
			memcpy(data,name->getData(),name->getDataSize());
			memcpy(data+name->getDataSize(),type->getData(),type->getDataSize());
		}
		return data;
	}
	size_t getDataSize(){
		if(!data_size){
			data_size+=name->getDataSize();
			data_size+=type->getDataSize();
		}
		return data_size;
	}
};

class rtmp_object:public rtmp_type{
public:
	byte end_marker[3];
	std::vector <rtmp_property*> properties; 

public:
	rtmp_object():rtmp_type(0x03){
		end_marker[0]=0x00;end_marker[1]=0x00;end_marker[2]=0x09;
		
	}
	~rtmp_object(){
		for(int i=0;i<properties.size();i++)
			delete properties[i];
		
	}
	void addProperty(rtmp_property *p){
		properties.push_back(p);
		data_size=0;	
	}
	byte* getData(){
		if(!data){
			data=new byte[getDataSize()+1+3];
			memcpy(data,&amf_type,1);			
			size_t offset=1;
			for(int i=0;i<properties.size();i++)
			{
				memcpy(data+offset,properties[i]->getData(),properties[i]->getDataSize());
				offset+=properties[i]->getDataSize();
			}
			memcpy(data+offset,end_marker,3);
		}
		return data;
	}
	size_t getDataSize(){
		if(!data_size){
			data_size+=1;
			for(int i=0;i<properties.size();i++)
				data_size+=properties[i]->getDataSize();
			data_size+=3;
		}
		return data_size;	
	}
		
	static rtmp_object* parse_object(byte* data){

		return 0;		
	}

};

class rtmp_raw_data:public rtmp_type{
public:

	rtmp_raw_data(byte* val,size_t size):rtmp_type(0x00){
		data=new byte[size];
		memcpy(data,val,size);
		data_size=size;	
	}
	~rtmp_raw_data(){}
	byte* getData(){return data;}
	size_t getDataSize(){return data_size;}

};

class rtmp_header{
public:
	byte fmt_csid;
	byte timestamp[3];
	byte body_size[3];
	byte type_id;
	byte stream_id[4];

	byte* data;
	size_t data_size;
public:
	rtmp_header(byte header_type=0x00,byte cstream_id=0x03,ulong timestamp=0x00,
		    size_t body_size=0x00,byte type_id=0x14,ulong stream_id=0x00):data(0),data_size(0){		
		fmt_csid=set_rtmp_basic_header(header_type,cstream_id);
		memset(this->timestamp,0,3);
		tohex(this->timestamp,3,(ulong)timestamp);
		memset(this->body_size,0,3);
		tohex(this->body_size,3,(ulong)body_size);
		this->type_id=type_id; //amf0 c0mmand;
		memset(this->stream_id,0,4);
		tohex(this->stream_id,4,(ulong)stream_id);
	}
	void setHeaderType(byte header_type){}
	void setCStreamId(byte cstream_id){}
	void setTimeStamp(ulong timestamp){tohex(this->timestamp,3,(ulong)timestamp);}
	void setBodySize(size_t body_size){tohex(this->body_size,3,(ulong)body_size);}
	void setTypeId(byte type_id){this->type_id=type_id;}
	~rtmp_header(){if(data) delete[] data;}
	byte* getData(){
		if(!data){
			data= new byte[getDataSize()];
			switch(fmt_csid>>6){
				case(rtmp_header_type0):
					memcpy(data,&fmt_csid,sizeof(fmt_csid));
					memcpy(data+1,timestamp,3);
					memcpy(data+3+1,body_size,3);
					memcpy(data+6+1,&type_id,1);
					memcpy(data+7+1,stream_id,4);
				break;
				case(rtmp_header_type1):
					memcpy(data,&fmt_csid,sizeof(fmt_csid));
					memcpy(data+1,timestamp,3);
					memcpy(data+3+1,body_size,3);
					memcpy(data+6+1,&type_id,1);
				break;
				case(rtmp_header_type2):
					memcpy(data,&fmt_csid,sizeof(fmt_csid));
					memcpy(data+1,timestamp,3);					
				break;
				case(rtmp_header_type3):
					memcpy(data,&fmt_csid,sizeof(fmt_csid));
				break;	
				default: delete [] data; exit(-1);
			}
			
		}
		return data;
	}
	size_t getDataSize(){
		if(!data_size){
			switch(fmt_csid>>6){
				case(rtmp_header_type0):
					data_size=sizeof(fmt_csid)+sizeof(timestamp)+
						  sizeof(body_size)+sizeof(type_id)+
						  sizeof(stream_id);
				break;
				case(rtmp_header_type1):
					data_size=sizeof(fmt_csid)+sizeof(timestamp)+
						  sizeof(body_size)+sizeof(type_id);
				break;
				case(rtmp_header_type2):
					data_size=sizeof(fmt_csid)+sizeof(timestamp);
				break;
				case(rtmp_header_type3):
					data_size=sizeof(fmt_csid);
				break;
				default: exit(-1);
			}
			
		}
		return data_size;
	}
	static rtmp_header* parseHeader(int &sockfd){
		byte _fmt_csid;
		int r=recvall(sockfd,&_fmt_csid,1,0);
		if(!r) return 0;
		rtmp_header *pheader=0;
		switch(_fmt_csid>>6){
			case(rtmp_header_type0):{
				byte buff[11];
				r=recvall(sockfd,buff,11,0);
				if(!r) return 0;
				pheader=new rtmp_header(_fmt_csid>>6,_fmt_csid&0x03);
				memcpy(pheader->timestamp,buff,3);
				memcpy(pheader->body_size,buff+3,3);
				memcpy(&pheader->type_id,buff+6,1);
				memcpy(pheader->stream_id,buff+7,4);
			break;}
			case(rtmp_header_type1):{
				byte buff[7];
				r=recvall(sockfd,buff,7,0);
				if(!r) return 0;
				pheader=new rtmp_header(_fmt_csid>>6,_fmt_csid&0x03);
				memcpy(pheader->timestamp,buff,3);
				memcpy(pheader->body_size,buff+3,3);
				memcpy(&pheader->type_id,buff+6,1);
			break;}
			case(rtmp_header_type2):{
				byte buff[3];
				r=recvall(sockfd,buff,3,0);
				if(!r) return 0;
				pheader=new rtmp_header(_fmt_csid>>6,_fmt_csid&0x03);
				memcpy(pheader->timestamp,buff,3);				
			break;}
			case(rtmp_header_type3):
				pheader=new rtmp_header(_fmt_csid>>6,_fmt_csid&0x03);				
			break;
			default: return 0;
		}
		
		return pheader;
		
	}
};



class rtmp_body{
public:
	std::vector <rtmp_type*> types;
	byte* data;
	size_t data_size;
	size_t chunk_size;
	size_t offset;
public:
	rtmp_body():data(0),data_size(0),chunk_size(rtmp_chunk_size),offset(0){
		
	}	
	~rtmp_body(){
		for(size_t i=0;i<types.size();i++)
			delete types[i];
		if(data) delete[] data;
	}
	void addType(rtmp_type *t){
		types.push_back(t);
		data_size=0;
	}
	byte* getData(){
		if(!data)
		{
			size_t offset=0;
			data=new byte[getDataSize()];
			for(size_t i=0;i<types.size();i++)
			{
				memcpy(data+offset,types[i]->getData(),types[i]->getDataSize());
				offset+=types[i]->getDataSize();
			}
		}		
		return data;
	}
	size_t getDataSize(){
		if(!data_size)
		{
			for(size_t i=0;i<types.size();i++)
				data_size+=types[i]->getDataSize();			
		}
		return data_size;
	}
	int getNextChunk(byte** chunk)
	{
		int chunk_data_size=0;
		if(offset==getDataSize()) {return 0;}
		
		if(getDataSize()-offset>=chunk_size){
			chunk_data_size=chunk_size;		
		}
		else
		{
			chunk_data_size=getDataSize()-offset;
		}
		
		*chunk=getData()+offset;
		offset+=chunk_data_size;
		
		return chunk_data_size;
	}
	
	void reset(){offset=0;}
	static rtmp_body* parseBody(int &sockfd,rtmp_header *pheader){
		
		if(!pheader) return 0;
		ulong body_size=fromhex(pheader->body_size,3);	
		byte buff[body_size];
		int r=recvall(sockfd,buff,body_size,0);
		
		if(!r) return 0;

		rtmp_body* pbody=new rtmp_body;
		
		switch(pheader->type_id)
		{
			case(rtmp_set_chunk_size):{
				pbody->addType(new rtmp_raw_data(buff,body_size));
				ulong val=fromhex(pbody->types[0]->getData(),pbody->types[0]->getDataSize());
				printf("set chunk size <--: %lu\n",val);
			break;}
			case(rtmp_user_ctrl_msg):{
				pbody->addType(new rtmp_raw_data(buff,body_size));
				ulong val=fromhex(pbody->types[0]->getData(),pbody->types[0]->getDataSize());
				printf("user control msg <--: ");
				switch(val){
					case(rtmp_ctrl_msg_sbegin):{
						printf("Stream Begin(%lu)\n",val);	
					break;}
				}
			break;}
			case(rtmp_window_ack):{
				pbody->addType(new rtmp_raw_data(buff,body_size));
				ulong val=fromhex(pbody->types[0]->getData(),pbody->types[0]->getDataSize());
				printf("window acknowledgement <--: %lu\n",val);
			
			break;}
			case(rtmp_set_bandwidth):{
				pbody->addType(new rtmp_raw_data(buff,4));
				pbody->addType(new rtmp_raw_data(buff+4,1));
				ulong val1=fromhex(pbody->types[0]->getData(),pbody->types[0]->getDataSize());
				ulong val2=fromhex(pbody->types[1]->getData(),pbody->types[1]->getDataSize());
				printf("set peer bandwidth <--: %lu, Dynamic(%lu)\n",val1,val2);
			break;}
			case(rtmp_amf0_command):{
				int amf_type=(int)buff[0];
				
				switch(amf_type){
					case(rtmp_amf_number):{
						pbody->addType(rtmp_number::parse_number(buff+1+pbody->getDataSize()));
					break;}
					case(rtmp_amf_boolean):{
						pbody->addType(rtmp_boolean::parse_boolean(buff+1pbody->getDataSize()));
					break;}
					case(rtmp_amf_string):{
						pbody->addType(rtmp_boolean::parse_string(buff+1+pbody->getDataSize()));
					break;}
					case(rtmp_amf_object):{
						pbody->addType(rtmp_boolean::parse_object(buff+1+pbody->getDataSize()));
					break;}
					case(rtmp_amf_null):{
						pbody->addType(rtmp_boolean::parse_null()+1+pbody->getDataSize());
					break;}
				}				
			break;}
		}	
		return pbody;		
	}
};


void dump_hex(const byte *data,int size)
{
	for(int i=0;i<size;i++)
	{
		printf("%02x",data[i]);
	}
}



int sendall(const int &sockfd,const byte *data,const size_t data_size, int flag=0)
{
	size_t sent_size=0;
	size_t left2send=data_size;
	size_t sent_size_total=0;
	
	while((sent_size=send(sockfd,data+(data_size-left2send),left2send,flag))<left2send)
	{
		left2send-=sent_size;
		sent_size_total+=sent_size;
	}
	sent_size_total+=sent_size;
	return sent_size_total;
}

int recvall(const int &sockfd,byte* data,const size_t data_size,int flag=0)
{
	size_t recv_size=0;
	size_t left2recv=data_size;
	size_t recv_size_total=0;	

	while((recv_size=recv(sockfd,data+(data_size-left2recv),left2recv,flag))<left2recv)
	{
		left2recv-=recv_size;
		recv_size_total+=recv_size;
	}
	recv_size_total+=recv_size;
	return recv_size_total;
}

int rtmp_init_handshake()
{
	struct sockaddr_in dest;
	
	struct hostent* host;
	h_0 C0=3,S0;
	h_1 C1,S1;
	h_2 C2,S2;
	
	if((sockfd = socket(AF_INET,SOCK_STREAM,0))<0){
		perror("Socket");
		exit(errno);	
	}
	// where socketfd is the socket you want to make non-blocking
	/*int status = fcntl(sockfd, F_SETFL, fcntl(sockfd, F_GETFL, 0) | O_NONBLOCK);

	if (status == -1){
  		perror("calling fcntl");
  		// handle the error.  By the way, I've never seen fcntl fail in this way
	}*/
	memset(&dest,0,sizeof(dest));
	dest.sin_family=AF_INET;
	dest.sin_port =htons(server_port);

	host=gethostbyname(server_addr);

	dest.sin_addr = *(struct in_addr *)host->h_addr_list[0];
	
	if(connect(sockfd,(struct sockaddr*)&dest,sizeof(dest))!=0/*&&errno!=EINPROGRESS*/){
		perror("Connect");
		exit(errno);	
	}



	memset(&C1,0,sizeof(C1));
	memset(((byte*)&C1)+8,'a',128);
	memset(&C2,0,sizeof(C2));
	memset(&S1,0,sizeof(S1));
	memset(&S2,0,sizeof(S2));

	int c0=send(sockfd,(byte*)&C0,sizeof(C0),0);		
	
	int c1=send(sockfd,(byte*)&C1,sizeof(C1),0);

	printf("C0+C1 -->%d %d\n",c0,c1);
	int s0=recvall(sockfd,(byte*)&S0,sizeof(S0),0);
	
	int s1=recvall(sockfd,(byte*)&S1,sizeof(S1),0);
	
	int s2=recvall(sockfd,(byte*)&S2,sizeof(S2),0);	
	printf("S0+S1+S2 <--:%d %d %d\n",s0,s1,s2);	
	
	memcpy(&C2,&S1,sizeof(S1));
	int c2=send(sockfd,(byte*)&C2,sizeof(C2),0);
	printf("C2 -->:%d\n",c2);

	return 0;
}

int rtmp_close_connection()
{
	close(sockfd);
	return 0;
}

int rtmp_connect()
{

	
	rtmp_header rheader(rtmp_header_type0,0x03,0x00,0x00,rtmp_amf0_command,0x00);
	rheader.setTimeStamp(0);

	
	rtmp_body rbody;
	rbody.addType(new rtmp_string("connect",7));
	rbody.addType(new rtmp_number(1));
	rbody.addType(new rtmp_object());
	((rtmp_object*)rbody.types[2])->addProperty(new rtmp_property(new rtmp_name("videoCodecs",11),new rtmp_number(252)));
	((rtmp_object*)rbody.types[2])->addProperty(new rtmp_property(new rtmp_name("audioCodecs",11),new rtmp_number(3191)));
	((rtmp_object*)rbody.types[2])->addProperty(new rtmp_property(new rtmp_name("flashVer",8),new rtmp_string("LNX 10,0,32,18",14)));
	((rtmp_object*)rbody.types[2])->addProperty(new rtmp_property(new rtmp_name("app",3),new rtmp_string("mtg",3)));		
	((rtmp_object*)rbody.types[2])->addProperty(new rtmp_property(new rtmp_name("tcUrl",5),new rtmp_string("rtmp://mtg.babahh.com:1935/mtg?id=mp4:tv3/superstaar15/saated/10081603/10081603.mp4",83)));
	((rtmp_object*)rbody.types[2])->addProperty(new rtmp_property(new rtmp_name("videoFunction",13),new rtmp_number(1)));
	((rtmp_object*)rbody.types[2])->addProperty(new rtmp_property(new rtmp_name("capabilities",12),new rtmp_number(15)));	
	((rtmp_object*)rbody.types[2])->addProperty(new rtmp_property(new rtmp_name("fpad",4),new rtmp_boolean(0)));		
	((rtmp_object*)rbody.types[2])->addProperty(new rtmp_property(new rtmp_name("swfUrl",6),new rtmp_null()));
	((rtmp_object*)rbody.types[2])->addProperty(new rtmp_property(new rtmp_name("objectEncoding",14),new rtmp_number(0)));
	
	rheader.setBodySize(rbody.getDataSize());
	
	printf("connect -->:%ld\n",rheader.getDataSize()+rbody.getDataSize());	
	int rh=send(sockfd,rheader.getData(),rheader.getDataSize(),0);		
	rtmp_header c3(rtmp_header_type3,0x03);
	byte* chunk;
	int cds;
	while((cds=rbody.getNextChunk(&chunk)))
	{
		send(sockfd,chunk,cds,0);
		if(cds==rtmp_chunk_size)
		send(sockfd,c3.getData(),c3.getDataSize(),0);		
	}
	
	
	rtmp_header* 	pheader;
	rtmp_body*	pbody;
	int expected_packetsn=2;
	while(expected_packetsn)
	{
		pheader=rtmp_header::parseHeader(sockfd);
		pbody=rtmp_body::parseBody(sockfd,pheader);
		delete pbody;
		delete pheader;
		expected_packetsn--;
	}
	return 0;
}

int main(int argc, char** argv){

	rtmp_init_handshake();
	rtmp_connect();
	rtmp_close_connection();
	
	

	

	
	

	return 0;
}
